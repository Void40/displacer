package void4.displacer

import net.minecraft.block.entity.{BlockEntity, ChestBlockEntity}
import net.minecraft.block.{BlockEntityProvider, BlockState, ChestBlock, InventoryProvider}
import net.minecraft.entity.{EntityType, ItemEntity}
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.inventory.{Inventories, Inventory, SidedInventory}
import net.minecraft.item.ItemStack
import net.minecraft.nbt.CompoundTag
import net.minecraft.state.property.Properties
import net.minecraft.util.Tickable
import net.minecraft.util.collection.DefaultedList
import net.minecraft.util.math.{BlockPos, Box, Direction, Vec3i}

import scala.jdk.javaapi.CollectionConverters
import scala.util.control.Breaks

class DisplacerBlockEntity extends BlockEntity(Main.BE_TYPE) with SidedInventory with Tickable {
	private val inv = DefaultedList.ofSize(1, ItemStack.EMPTY)
	private var prev = false
	private var cd = 0

	private def i1: ItemStack = getStack(0)
	private def isFull = !isEmpty && Main.full(i1)
	override def getAvailableSlots(side: Direction) = Array(0)
	override def canInsert(slot: Int, stack: ItemStack, dir: Direction) = true
	override def canExtract(slot: Int, stack: ItemStack, dir: Direction) = true
	override def size = 1
	override def isEmpty = i1.isEmpty
	override def getStack(slot: Int) = inv.get(slot)
	override def removeStack(slot: Int) = Inventories.removeStack(inv, slot)
	override def canPlayerUse(player: PlayerEntity) = false
	override def clear() = inv.clear()

	private def compatible(i1: ItemStack, i2: ItemStack) = {
		i1.getItem == i2.getItem && i1.getDamage == i2.getDamage && ItemStack.areTagsEqual(i1, i2)
	}

	private def acceptWithRemainder(i: ItemEntity) = {
		val i2 = i.getStack
		if(isEmpty) {
			setStack(0, i2.copy())
			i.remove()
			true
		}
		else {
			if(!compatible(i2, i1)) false
			else {
				val c1 = i1.getCount
				val c2 = i2.getCount
				val delta = Math.min(i1.getMaxCount - c1, c2)
				i1.setCount(c1 + delta)
				if(delta == c2) i.remove()
				else i2.setCount(c2 - delta)
				true
			}
		}
	}

	private def dec() = {
		i1.decrement(1)
		if(i1.getCount == 0) clear()
	}

	private def getTargetInv(pos2: BlockPos) = {
		val state = world.getBlockState(pos2)
		val block = state.getBlock
		block match {
			case ip: InventoryProvider => Some(ip.getInventory(state, world, pos2))
			case _: BlockEntityProvider => world.getBlockEntity(pos2) match {
				case inv2: Inventory => Some(inv2)
				case _: ChestBlockEntity => block match {
					case cb: ChestBlock => Some(ChestBlock.getInventory(cb, state, world, pos2, true))
					case _ => None
				}
				case _ => None
			}
			case _ => None
		}
	}

	private def slots(inv: Inventory, dir: Direction) = {
		inv match {
			case si: SidedInventory => si.getAvailableSlots(dir)
			case _ => Array.range(0, inv.size)
		}
	}

	private def tryInsert(slot: Int, inv: Inventory, dir: Direction): Boolean = {
		val i3 = i1.copy()
		i3.setCount(1)
		inv match {
			case si: SidedInventory => if(!si.canInsert(slot, i3, dir)) return false
			case _ =>
		}
		val i2 = inv.getStack(slot)
		if(i2.isEmpty) {
			dec()
			inv.setStack(slot, i3)
			true
		}
		else if(compatible(i1, i2) && !Main.full(i2)) {
			dec()
			i2.increment(1)
			true
		}
		else false
	}

	private def succ() = {
		if(isFull) false
		else {
			val f = (i: ItemEntity) => pos.getSquaredDistance(i.getX, i.getY, i.getZ, false)
			val ord = Ordering.fromLessThan((i1: ItemEntity, i2: ItemEntity) => f(i1) < f(i2))
			val reg = DisplacerBlockEntity.succRegion(world.getBlockState(pos), pos)
			val temp = world.getEntitiesByType[ItemEntity](EntityType.ITEM, reg, _ => true)
			val items = CollectionConverters.asScala(temp).sorted(ord)
			if(items.isEmpty) false
			else items.map((i: ItemEntity) => acceptWithRemainder(i)).foldRight(false)((x, y) => x || y)
		}
	}

	private def push(): Unit = {
		if(isEmpty) return
		val dir = world.getBlockState(pos).get(Properties.FACING)
		val loop = new Breaks
		for(by <- 1 to 3) {
			loop.breakable {
				val pos2: BlockPos = pos.offset(dir.getOpposite, by)
				val inv = getTargetInv(pos2) match {
					case Some(x) => x
					case None => loop.break()
				}
				for(i: Int <- slots(inv, dir)) if(tryInsert(i, inv, dir)) return
			}
		}
	}

	override def fromTag(state: BlockState, tag: CompoundTag) = {
		super.fromTag(state, tag)
		prev = tag.getBoolean("prev")
		cd = tag.getByte("cooldown")
		Inventories.fromTag(tag, inv)
	}

	override def toTag(tag: CompoundTag) = {
		Inventories.toTag(tag, inv)
		tag.putBoolean("prev", prev)
		tag.putByte("cooldown", cd.asInstanceOf[Byte])
		super.toTag(tag)
	}

	override def removeStack(slot: Int, amount: Int) = {
		val stack = Inventories.splitStack(inv, slot, amount)
		if(!stack.isEmpty) markDirty()
		stack
	}

	override def setStack(slot: Int, stack: ItemStack) = {
		inv.set(slot, stack)
		markDirty()
	}

	override def tick(): Unit = {
		if(world == null || world.isClient) return
		val curr = world.getReceivedRedstonePower(pos) > 0
		if(curr) {
			if(prev) push()
			else if(cd < 1 && succ()) cd = DisplacerBlockEntity.COOLDOWN
		}
		prev = curr
		if(cd > 0) cd -= 1
		markDirty()
	}
}

object DisplacerBlockEntity {
	val COOLDOWN = 40

	private def succRegion(state: BlockState, pos: BlockPos) = {
		val dir = state.get(Properties.FACING)
		val corners = dir match {
			case Direction.NORTH => (new Vec3i(-1, -1, -3), new Vec3i(2, 2, 0))
			case Direction.EAST => (new Vec3i(1, -1, -1), new Vec3i(4, 2, 2))
			case Direction.SOUTH => (new Vec3i(-1, -1, 1), new Vec3i(2, 2, 4))
			case Direction.WEST => (new Vec3i(-3, -1, -1), new Vec3i(0, 2, 2))
			case Direction.UP => (new Vec3i(-1, 1, -1), new Vec3i(2, 4, 2))
			case Direction.DOWN => (new Vec3i(-1, -3, -1), new Vec3i(2, 0, 2))
		}
		new Box(pos.add(corners._1), pos.add(corners._2))
	}
}