package void4.displacer

import net.fabricmc.api.ModInitializer
import net.minecraft.block.entity.BlockEntityType
import net.minecraft.item.{BlockItem, Item, ItemGroup, ItemStack}
import net.minecraft.util.Identifier
import net.minecraft.util.registry.Registry

object Main extends ModInitializer {
	val ID = new Identifier("displacer", "displacer")
	val BLOCK = new DisplacerBlock
	val BE_TYPE = BlockEntityType.Builder.create(() => new DisplacerBlockEntity, BLOCK).build(null)

	def id[T](t: T) = t
	def full(i: ItemStack) = i.getCount == i.getMaxCount

	override def onInitialize() = {
		Registry.register(Registry.BLOCK, ID, BLOCK)
		Registry.register(Registry.ITEM, ID, new BlockItem(BLOCK, new Item.Settings().group(ItemGroup.REDSTONE)))
		Registry.register(Registry.BLOCK_ENTITY_TYPE, ID, BE_TYPE)
	}
}