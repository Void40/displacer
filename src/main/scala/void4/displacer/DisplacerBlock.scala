package void4.displacer

import net.fabricmc.fabric.api.`object`.builder.v1.block.FabricBlockSettings
import net.minecraft.block._
import net.minecraft.entity.ai.pathing.NavigationType
import net.minecraft.inventory.{Inventory, SidedInventory}
import net.minecraft.item.ItemPlacementContext
import net.minecraft.screen.ScreenHandler
import net.minecraft.state.StateManager
import net.minecraft.state.property.Properties
import net.minecraft.util.ItemScatterer
import net.minecraft.util.math.{BlockPos, Direction}
import net.minecraft.util.shape.VoxelShapes
import net.minecraft.world.{BlockView, World, WorldAccess}

class DisplacerBlock extends Block(DisplacerBlock.SETTINGS) with BlockEntityProvider with InventoryProvider {
	setDefaultState(getStateManager.getDefaultState.`with`(Properties.FACING, Direction.UP))

	override def hasComparatorOutput(state: BlockState) = true
	override def appendProperties(builder: StateManager.Builder[Block, BlockState]) = builder.add(Properties.FACING)
	override def canPathfindThrough(state: BlockState, world: BlockView, pos: BlockPos, `type`: NavigationType) = false
	override def createBlockEntity(world: BlockView) = new DisplacerBlockEntity

	private def applyToBE[U >: DisplacerBlockEntity, V](world: WorldAccess, pos: BlockPos, f: U => V, default: V) = {
		world.getBlockEntity(pos) match {
			case disp: DisplacerBlockEntity => f(disp.asInstanceOf[U])
			case _ => default
		}
	}

	override def onStateReplaced(state: BlockState, world: World, pos: BlockPos, state2: BlockState, m: Boolean) = {
		if(!state.isOf(state2.getBlock)) {
			applyToBE(world, pos, (i: Inventory) => ItemScatterer.spawn(world, pos, i), ())
			world.updateComparators(pos, this)
		}
		super.onStateReplaced(state, world, pos, state2, m)
	}

	override def getComparatorOutput(state: BlockState, world: World, pos: BlockPos) = {
		applyToBE(world, pos, (i: Inventory) => ScreenHandler.calculateComparatorOutput(i), 0)
	}

	override def getOutlineShape(state: BlockState, world: BlockView, pos: BlockPos, context: ShapeContext) = {
		state.get(Properties.FACING) match {
			case Direction.NORTH => DisplacerBlock.SHAPE_N
			case Direction.EAST => DisplacerBlock.SHAPE_E
			case Direction.SOUTH => DisplacerBlock.SHAPE_S
			case Direction.WEST => DisplacerBlock.SHAPE_W
			case Direction.UP => DisplacerBlock.SHAPE_U
			case Direction.DOWN => DisplacerBlock.SHAPE_D
		}
	}

	override def getPlacementState(ctx: ItemPlacementContext) = {
		getStateManager.getDefaultState.`with`(Properties.FACING, ctx.getPlayerLookDirection)
	}

	override def getInventory(state: BlockState, world: WorldAccess, pos: BlockPos) = {
		applyToBE(world, pos, Main.id[SidedInventory], null)
	}
}

object DisplacerBlock {
	private val MATS = (Material.METAL, MaterialColor.MAGENTA)
	private val SETTINGS = FabricBlockSettings.of(MATS._1, MATS._2).strength(6.0F, 8.0F).requiresTool()
	private val SHAPE_N = cs(6, 6, 10, 10, 10, 13, 2, 2, 4, 14, 14, 10)
	private val SHAPE_E = cs(3, 6, 6, 6, 10, 10, 6, 2, 2, 12, 14, 14)
	private val SHAPE_S = cs(6, 6, 3, 10, 10, 6, 2, 2, 6, 14, 14, 12)
	private val SHAPE_W = cs(10, 6, 6, 13, 10, 10, 4, 2, 2, 10, 14, 14)
	private val SHAPE_U = cs(6, 3, 6, 10, 6, 10, 2, 6, 2, 14, 12, 14)
	private val SHAPE_D = cs(6, 10, 6, 10, 13, 10, 2, 4, 2, 14, 10, 14)

	private def cs(a: Int, b: Int, c: Int, d: Int, e: Int, f: Int, g: Int, h: Int, i: Int, j: Int, k: Int, l: Int) = {
		VoxelShapes.union(Block.createCuboidShape(a, b, c, d, e, f), Block.createCuboidShape(g, h, i, j, k, l))
	}
}