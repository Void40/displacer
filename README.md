# Displacer

## Description
The Displacer (displacer:displacer) is a block and redstone component that can
suck items out of the world in a 3x3 region (like a vacuum hopper) and push to
inventories up to 3 blocks behind it (like a hopper duct, but with additional
range). It *cannot* extract from inventories, unlike a hopper. It has a single
inventory slot, but no GUI (since a GUI isn't really necessary here).

* Suck rate: up to a stack every 40 ticks
* Push rate: up to 1 item every tick (8x hopper speed)
* Inventory size: 1 stack

## Behaviour
(LOW  => redstone signal strength = 0)

(HIGH => redstone signal strength > 0)

* When it has a LOW signal:
  *	It will do absolutely nothing.
* When it has a HIGH signal, but was LOW the previous game tick:
  * It will attempt to suck items out of the world. It will not push in this
	tick. Items closer to the Displacer have higher priority. If it succeeds (ie
	it picks up at least one item), it will start a 2 second (40 ticks) cooldown,
	during which it will be able to push as usual but will not be able to suck
	any more items. (This is to discourage laggy setups).
* When it has a HIGH signal, and was HIGH the previous game tick:
  * It will attempt to push to inventories behind it. It will push 1 item every
	tick. Closer inventories have higher priority and it ignores anything else in
	the way. If it finds an inventory in the first two blocks but it refuses to
	accept an item, the Displacer will try to look behind it for another
	inventory in the same tick. It will not interact with entities like minecarts
	even if they have an inventory.
	
The Displacer provides a comparator output as you would expect. You will
probably need to slow down its transport until you get the comparator signal
since otherwise it will be too fast for the comparator to catch.